import React from 'react';
import ReactDOM from "react-dom/client";
import App from "./App";
import "./styles/index.scss";
//Redux Part
import {Provider } from "react-redux";
import {applyMiddleware,configureStore} from "@reduxjs/toolkit"
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import rootReducer from "./reducers"

const root = ReactDOM.createRoot(document.getElementById("root"));


//Reducer c'est celui qui se charge de mettre a jour notre espace de stockage
//Dossier Action : Récupere les actions de nos utilisateurs pour ensuite les interpreter
const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== "production",
});


root.render(
  <Provider store={store}>
    <App />
  </Provider>
);
