//Composants qui permettra d'incrémenter toutes nos données.
//C'est lui qui permet de mettre le state au plus haut niveau de notre applicationWeb.
import { combineReducers } from "@reduxjs/toolkit";
import stableReducer from "./stable.reducer";
import listReducer from "./list.reducer";

export default combineReducers({
  stableReducer,
  listReducer,
});
