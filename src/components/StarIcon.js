import React from "react";
import { useEffect } from "react";
import { useState } from "react";

const StarIcon = ({ coinId }) => {
  //console.log("coinIdProps", coinId);
  const [like, setLike] = useState(false);
  useEffect(() => {
    if (window.localStorage.coinList) {
      let favList = window.localStorage.coinList.split(",");
      if (favList.includes(coinId)) {
        setLike(true);
      }
    } else {
      setLike(false);
    }
  });
  const idChecker = (id) => {
    let favList = window.localStorage.coinList?.split(",");

    if (favList.includes(id)) {
      //Si l'ID est déjà existant c'est que l'utilisateur veut en faite le supprimer
      console.log("ID déjà existant, supression");
      console.log(
        "Etat du localStorage aant suppresion  : " +
          window.localStorage.coinList
      );
      window.localStorage.coinList = favList.filter((idCoin) => idCoin != id);
      console.log("Etat du localStorage  : " + window.localStorage.coinList);
      setLike(false);
    } else {
      //L'utilisateur veut renseigner l'ID en <favori></favori>
      console.log("Ajout de l'ID....");
      console.log(
        "Etat du localStorage avant ajout  : " + window.localStorage.coinList
      );
      window.localStorage.coinList = [...favList, id];
      console.log("Etat du localStorage  : " + window.localStorage.coinList);
      setLike(true);
      // let arr=["a","b","c","d","e"]
    }
  };
  return (
    <img
      onClick={() => {
        idChecker(coinId);
      }}
      src={like ? "./assets/star-full.svg" : "./assets/star-empty.svg"}
      alt="icon-star"
    />
  );
};

export default StarIcon;
