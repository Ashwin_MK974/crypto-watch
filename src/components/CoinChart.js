import axios from "axios";
import React, { useEffect, useState } from "react";
import {
  Area,
  AreaChart,
  CartesianAxis,
  CartesianGrid,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import colors from "../styles/index.scss";
const CoinChart = ({ coinId, coinName }) => {
  //console.clear();
  console.log("CoinId : " + coinId);
  console.log("Coinname : " + coinName);
  const [duration, setDuration] = useState(30);
  const [coinData, setCoinData] = useState([]);
  const headerData = [
    [1, "1 jour"],
    [3, "3 jours"],
    [7, "7 jours"],
    [30, "1 mois"],
    [91, "3 mois"],
    [181, "6 mois"],
    [365, "1 an"],
    [3000, "Max"],
  ];
  useEffect(() => {
    console.clear();
    let dataArray = [];
    axios
      .get(
        `https://api.coingecko.com/api/v3/coins/${coinId}/market_chart?vs_currency=usd&days=${duration}${
          duration > 32 ? "&interval=daily" : ""
        }`
      )
      .then((res) => {
        console.log(
          `https://api.coingecko.com/api/v3/coins/${coinId}/market_chart?vs_currency=usd&days=${duration}${
            duration > 32 ? "&interval=daily" : "" //Interval de jour et non pas un interval de jour ou de demi-journé
          }`
        );
        //console.log(res);
        console.log(res.data.prices);
        for (let i = 0; i <= res.data.prices.length - 1; i++) {
          let price = res.data.prices[i][1];
          let date = new Date(res.data.prices[i][0]).toLocaleDateString();

          dataArray.push({
            date,
            price,
          });

          if (i == res.data.prices.length - 1) {
            console.log(dataArray);
            setCoinData(dataArray);
            console.log(coinData);
          }
        }
      });
  }, [coinId, duration]);
  return (
    <div className="coin-chart">
      <h1>{coinName}</h1>
      <div className="btn-container">
        {headerData.map((el, i) => (
          <div
            key={el[0]}
            htmlFor={"btn" + el[0]}
            onClick={() => {
              setDuration(el[0]);
            }}
            //Parmis tout les el[0] si il y'en a un qui correspond a duration alors on définie la classe active-btn
            className={el[0] === duration ? "active-btn" : " "}
          >
            {el[1]}
          </div>
        ))}
      </div>
      <AreaChart
        width={680}
        height={250}
        data={coinData}
        margin={{ top: 10, right: 0, left: 100, bottom: 0 }}
      >
        <XAxis dataKey="date" />
        <YAxis domain={["auto", "auto"]} />
        <CartesianGrid strokeDasharray="3 3" /> //Affichage des barre verticale
        <Area
          type="monotone"
          dataKey="price"
          stroke={colors.color1}
          fillOpacity={1}
          fill="url(#colorUv)"
        />
        <defs>
          <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
            <stop offset="7%" stopColor={colors.color1} stopOpacity={0.8} />
            <stop offset="93%" stopColor={colors.white1} stopOpacity={0} />
          </linearGradient>
        </defs>
      </AreaChart>
    </div>
  );
};

export default CoinChart;
