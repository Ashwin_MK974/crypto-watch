export const SET_STABLE_STATE = "SET_STABLE_STATE"; //On exporte la fonction qui nous permet d'afficher ou pas les stable coin

//Ici la fonction prend en argument un bolean true ou false que l'on va vouloir envoyer a redux.
export const setStableState = (bool) => {
  return (dispatch) => {
    return dispatch({ type: SET_STABLE_STATE, payload: bool });
    //Le type que l'on va envoyer au reducer est SET_STABLE_STATE
  };
};
